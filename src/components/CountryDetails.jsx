import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useTheme } from "./ThemeContext";
import { useParams, useNavigate } from "react-router-dom";
import whiteMoon from "/src/assets/moon-regular.svg";
import darkMoon from "/src/assets/moon-solid.svg";
import whiteArrow from "/src/assets/arrow-left-regular.svg";
import darkArrow from "/src/assets/arrow-left-solid.svg";
import "./CountryDetails.css";

const CountryDetails = () => {
  const { mode, toggleTheme } = useTheme();
  const { isDarkMode } = mode;
  const { id } = useParams();
  const navigate = useNavigate();
  const [country, setCountry] = useState(null);

  useEffect(() => {
    const apiUrl = `https://restcountries.com/v3.1/alpha/${id}`;
    fetch(apiUrl)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        setCountry(data[0]);
      })
      .catch((error) => {
        console.error("Error fetching country data:", error);
      });
  }, [id]);

  if (!country) {
    return <div>Loading...</div>;
  }

  const handleBorderCountry = (border) => {
    navigate(`/country/${border}`);
  };

  return (
    <>
      <header>
        <div
          className={isDarkMode ? "light-header" : "dark-header"}
          id="header"
        >
          <h2>Where in the world?</h2>
          <div className="mode" onClick={toggleTheme}>
            <img
              src={
                isDarkMode
                  ? whiteMoon
                  : darkMoon
              }
              alt=""
              className="icon"
            />
            <p>Dark Mode</p>
          </div>
        </div>
      </header>
      <article className={isDarkMode ? "light-grid" : "dark-grid"}>
        <div
          className={isDarkMode ? "light-back-button" : "dark-back-button"}
          id="back-button"
        >
          <Link to="/">
            <img
              src={
                isDarkMode
                  ? whiteArrow
                  : darkArrow
              }
            />
            Back
          </Link>
        </div>
        <div
          className={
            isDarkMode ? "light-country-details" : "dark-country-details"
          }
          id="country-details"
        >
          <div className="img-container">
            <img
              src={country.flags["png"]}
              alt={country.name.nativeName.common}
            />
          </div>
          <div className="country-info">
            <div className="country-inner-info1">
              <h2>{country.name.common}</h2>
              <p>
                <b>Native Name:</b>{" "}
                {
                  Object.values(country.name.nativeName)[
                    Object.values(country.name.nativeName).length - 1
                  ].common
                }
              </p>
              <p>
                <b>Population:</b> {country.population}
              </p>
              <p>
                <b>Region:</b> {country.region}
              </p>
              <p>
                <b>Subregion:</b> {country.subregion}
              </p>
              <p>
                <b>Capital:</b> {country.capital}
              </p>
              <p>
                <b>Area:</b> {country.area}
              </p>
            </div>
            <div className="country-inner-info2">
              <p>
                <b>Top LevelDomain:</b> {country.tld[0].slice(1)}
              </p>
              <p>
                <b>Currencies:</b> {Object.values(country.currencies)[0].name}
              </p>
              <p>
                <b>Languages:</b> {Object.values(country.languages).join(", ")}
              </p>
            </div>
            {country.borders && (
              <div
                className={isDarkMode ? "light-border" : "dark-border"}
                id="border"
              >
                <b>Border Countries:</b>
                {country.borders.map((border, index) => (
                  <button
                    key={index}
                    onClick={() => handleBorderCountry(border)}
                  >
                    {border}
                  </button>
                ))}
              </div>
            )}
          </div>
        </div>
      </article>
    </>
  );
};

export default CountryDetails;
