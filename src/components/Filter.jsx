import React from "react";
import { useTheme } from "./ThemeContext";
import "./Filter.css";

const Filter = ({
  filterRegion,
  setFilterRegion,
  filterSubRegion,
  setFilterSubRegion,
  subRegion,
  countries,
}) => {
  const { mode, toggleTheme } = useTheme();
  const { isDarkMode } = mode;

  const handleFilterChange = (event) => {
    const selectedRegion = event.target.value;
    setFilterRegion(selectedRegion);
  };

  const handleFilterSubChange = (event) => {
    const selectedSubRegion = event.target.value;
    setFilterSubRegion(selectedSubRegion);
  };

  const filteredSubRegions = Array.from(subRegion).filter((subregion) => {
    return (
      filterRegion === "" ||
      countries.some(
        (country) =>
          country.region.toLowerCase() === filterRegion.toLowerCase() &&
          country.subregion === subregion
      )
    );
  });

  return (
    <>
      <div className="filter-container">
        <div className="filter">
          <select
            className={isDarkMode ? "light-select" : "dark-select"}
            name="continent"
            id="continent"
            value={filterRegion}
            onChange={handleFilterChange}
          >
            <option value="filter-region" className="region-name">
              Filter by Region
            </option>
            <option value="africa" className="region-name">
              Africa
            </option>
            <option value="americas" className="region-name">
              America
            </option>
            <option value="asia" className="region-name">
              Asia
            </option>
            <option value="europe" className="region-name">
              Europe
            </option>
            <option value="oceania" className="region-name">
              Oceania
            </option>
          </select>
        </div>
        <div className="filter-sub-region">
          <select
            className={isDarkMode ? "light-select" : "dark-select"}
            name="subregion"
            id="subregion"
            value={filterSubRegion}
            onChange={handleFilterSubChange}
          >
            <option value="filter-region" className="sub-region">
              Filter by Subregion
            </option>
            {filteredSubRegions.map((subregion, index) => {
              if (subregion) {
                return (
                  <option key={index} value={subregion} className="sub-region">
                    {subregion}
                  </option>
                );
              }
            })}
          </select>
        </div>
      </div>
    </>
  );
};

export default Filter;
