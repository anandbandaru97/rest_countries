import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useTheme } from "./ThemeContext";
import Loader from "./Loader";
import Filter from "./Filter";
import Search from "./Search";
import Sort from "./Sort";
import whiteMoon from "/src/assets/moon-regular.svg";
import darkMoon from "/src/assets/moon-solid.svg";
import "./Countries.css";

const apiUrl = "https://restcountries.com/v3.1/all";

const Countries = () => {
  const { mode, toggleTheme } = useTheme();
  const { isDarkMode } = mode;
  const [countries, setCountries] = useState([]);
  const [searchCountry, setSearchCountry] = useState("");
  const [filterRegion, setFilterRegion] = useState("");
  const [filterSubRegion, setFilterSubRegion] = useState("");
  const [sortByPopulation, setSortByPopulation] = useState("");
  const [sortByArea, setSortByArea] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  const subRegion = [
    ...new Set(
      countries.map((country) => {
        if (country.region) {
          return country.subregion;
        }
      })
    ),
  ];

  useEffect(() => {
    fetch(apiUrl)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        setTimeout(() => {
          setCountries(data);
          setIsLoading(false);
          setError(null);
        }, 1500);
      })
      .catch((error) => {
        setError("Error:", error.message);
        // console.error("Error fetching data:", error);
      });
  }, []);

  const filteredCountries = countries
    .filter((country) => {
      const matchesSearch = country.name.common
        .toLowerCase()
        .includes(searchCountry.toLowerCase());
      const matchesRegion =
        filterRegion === "" || country.region.toLowerCase() === filterRegion;
      const matchesSubRegion =
        filterSubRegion === "" || country.subregion === filterSubRegion;

      return matchesSearch && matchesRegion && matchesSubRegion;
    })
    .sort((a, b) => {
      if (sortByPopulation === "asc") {
        return a.population - b.population;
      } else if (sortByPopulation === "desc") {
        return b.population - a.population;
      } else if (sortByArea === "asc") {
        return a.area - b.area;
      } else if (sortByArea === "desc") {
        return b.area - a.area;
      } else {
        return 0;
      }
    });

  return (
    <>
      <header>
        <div
          className={isDarkMode ? "light-header" : "dark-header"}
          id="header"
        >
          <h2>Where in the world?</h2>
          <div className="mode" onClick={toggleTheme}>
            <img
              src={
                isDarkMode
                  ? whiteMoon
                  : darkMoon
              }
              alt=""
              className="icon"
            />
            <p>Dark Mode</p>
          </div>
        </div>
      </header>

      <section className={isDarkMode ? "light-grid" : "dark-grid"}>
        {isLoading ? (
          <Loader />
        ) : (
          <div className="components">
            <Search
              searchCountry={searchCountry}
              setSearchCountry={setSearchCountry}
            />
            <Filter
              filterRegion={filterRegion}
              setFilterRegion={setFilterRegion}
              filterSubRegion={filterSubRegion}
              setFilterSubRegion={setFilterSubRegion}
              subRegion={subRegion}
              countries={countries}
            />
            <Sort
              sortByPopulation={sortByPopulation}
              setSortByPopulation={setSortByPopulation}
              sortByArea={sortByArea}
              setSortByArea={setSortByArea}
            />
          </div>
        )}

        <div className="main-container">
          {error ? (
            <div className="error-message">(error)</div>
          ) : filteredCountries == "" && searchCountry ? (
            <div className="empty-message">No such Countries found</div>
          ) : (
            filteredCountries.map((country) => {
              const {
                cca2,
                name,
                population,
                region,
                capital,
                flags,
                subregion,
                area,
              } = country;
              return (
                <Link to={`/${name.common}/${cca2}`} key={cca2}>
                  <div
                    className={
                      isDarkMode
                        ? "light-country-container"
                        : "dark-country-container"
                    }
                    id="country-container"
                  >
                    <img src={flags.png} alt={name} />
                    <div className="country-details">
                      <h3>{name.common}</h3>
                      <p>
                        <b>Population :</b> {population}
                      </p>
                      <p>
                        <b>Region :</b> {region}
                      </p>
                      <p>
                        <b>Subregion :</b> {subregion}
                      </p>
                      <p>
                        <b>Capital :</b> {capital}
                      </p>
                      <p>
                        <b>Area :</b> {area}
                      </p>
                    </div>
                  </div>
                </Link>
              );
            })
          )}
        </div>
      </section>
    </>
  );
};

export default Countries;
