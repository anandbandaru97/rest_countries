import React from "react";
import "./Loader.css";

const Loader = () => {
  return (
    <div className="loader">
      <div className="loading"></div>
      <span className="loading-text">Loading...</span>
    </div>
  );
};

export default Loader;
