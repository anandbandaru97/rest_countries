import React, { createContext, useContext, useReducer } from "react";

const ThemeContext = createContext();

const initialMode = {
  isDarkMode: false,
};

const themeReducer = (mode, action) => {
  switch (action.type) {
    case "TOGGLE_THEME":
      return { ...mode, isDarkMode: !mode.isDarkMode };
    default:
      return mode;
  }
};

export function ThemeProvider({ children }) {
  const [mode, dispatch] = useReducer(themeReducer, initialMode);

  const toggleTheme = () => {
    dispatch({ type: "TOGGLE_THEME" });
  };
  return (
    <ThemeContext.Provider value={{ mode, toggleTheme }}>
      {children}
    </ThemeContext.Provider>
  );
}

export function useTheme() {
  return useContext(ThemeContext);
}
