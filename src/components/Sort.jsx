import React from "react";
import { useTheme } from "./ThemeContext";
import "./Sort.css";

const Sort = ({
  sortByPopulation,
  setSortByPopulation,
  sortByArea,
  setSortByArea,
}) => {
  const { mode, toggleTheme } = useTheme();
  const { isDarkMode } = mode;

  const handleSortByPopulation = () => {
    if (sortByPopulation === "" || sortByPopulation === "asc") {
      setSortByPopulation("desc");
      setSortByArea("");
    } else {
      setSortByPopulation("asc");
    }
  };

  const handleSortByArea = () => {
    if (sortByArea === "" || sortByArea === "asc") {
      setSortByArea("desc");
      setSortByPopulation("");
    } else {
      setSortByArea("asc");
    }
  };

  return (
    <>
      <div className="sort-container">
        <div className="sort-population">
          <select
            className={isDarkMode ? "light-select" : "dark-select"}
            name="population"
            id="population"
            value={sortByPopulation}
            onChange={handleSortByPopulation}
          >
            <option value="asc">Population Asc</option>
            <option value="desc">Population Desc</option>
          </select>
        </div>
        <div className="sort-area">
          <select
            className={isDarkMode ? "light-select" : "dark-select"}
            name="area"
            id="area"
            value={sortByArea}
            onChange={handleSortByArea}
          >
            <option value="asc">Area Asc</option>
            <option value="desc">Area Desc</option>
          </select>
        </div>
      </div>
    </>
  );
};

export default Sort;
