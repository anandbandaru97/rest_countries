import React from "react";
import { useTheme } from "./ThemeContext";
import searchIcon from "/src/assets/magnifying-glass-solid.svg";
import "./Search.css";

const Search = ({ searchCountry, setSearchCountry }) => {
  const { mode, toggleTheme } = useTheme();
  const { isDarkMode } = mode;

  const handleSearchChange = (event) => {
    setSearchCountry(event.target.value);
  };
  return (
    <>
      <div className="search-container">
        <div className="search">
          <img
            src={searchIcon}
            alt=""
            className={isDarkMode ? "light-search-icon" : "dark-search-icon"}
            id="search-icon"
          />
          <input
            className={isDarkMode ? "light-input" : "dark-input"}
            type="search"
            name="search"
            id="search"
            value={searchCountry}
            onChange={handleSearchChange}
            placeholder="Search for a country..."
          ></input>
        </div>
      </div>
    </>
  );
};

export default Search;
