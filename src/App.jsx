import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Countries from "./components/Countries";
import CountryDetails from "./components/CountryDetails";
import { ThemeProvider } from "./components/ThemeContext";
import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <ThemeProvider>
              <Countries />
            </ThemeProvider>
          }
        />
        <Route
          path="/:country/:id"
          element={
            <ThemeProvider>
              <CountryDetails />
            </ThemeProvider>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
